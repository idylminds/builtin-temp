#ifndef _BUILTIN_TEMP_DOT_H_
#define _BUILTIN_TEMP_DOT_H_

#include "Arduino.h"

class BuiltinTemp
{
 public:
    double readCelsius();
    double readFarenheit();
};

#endif // _BUILTIN_TEMP_DOT_H_
