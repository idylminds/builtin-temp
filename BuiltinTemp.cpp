#include "Arduino.h"
#include "BuiltinTemp.h"

long readRaw()
{
  // https://code.google.com/p/tinkerit/wiki/SecretThermometer
  // Read temperature sensor against 1.1V reference
  #if defined(__AVR_ATmega32U4__)
    ADMUX = _BV(REFS1) | _BV(REFS0) | _BV(MUX2) | _BV(MUX1) | _BV(MUX0);
    ADCSRB = _BV(MUX5); // the MUX5 bit is in the ADCSRB register
  #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
    ADMUX = _BV(REFS1) | _BV(MUX5) | _BV(MUX1);
  #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
    ADMUX = _BV(REFS1) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1) | _BV(MUX0);
  #else
    ADMUX = _BV(REFS1) | _BV(REFS0) | _BV(MUX3);
  #endif

  delay(2); // Wait for ADMUX setting to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC))
    ; // measuring

  long result = ADCL | (ADCH << 8);
  return (result - 125) * 1075;
}

double celsiusToFarenheit(double value)
{
  return (value * 9.0) / 5.0 + 32.0;
}

double BuiltinTemp::readCelsius()
{
    double temp = (double) readRaw();

    return temp / 10000;
}

double BuiltinTemp::readFarenheit()
{
    double temp = (double) readRaw();

    return celsiusToFarenheit(temp / 10000);
}
